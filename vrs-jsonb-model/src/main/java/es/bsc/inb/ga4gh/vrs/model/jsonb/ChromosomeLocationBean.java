/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.jsonb;

import es.bsc.inb.ga4gh.vrs.model.ChromosomeLocation;
import jakarta.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class ChromosomeLocationBean 
        implements ChromosomeLocation<CURIEBean, CytobandIntervalBean>,
        LocationInterface {
    
    private CURIEBean id;
    private CURIEBean species_id;
    private String chromosome;
    private CytobandIntervalBean interval;
    
    @JsonbProperty("_id")
    @Override
    public CURIEBean getId() {
        return id;
    }
    
    @JsonbProperty("_id")
    @Override
    public void setId(CURIEBean id) {
        this.id = id;
    }
    
    @JsonbProperty("species_id")
    @Override
    public CURIEBean getSpeciesId() {
        return species_id;
    }
    
    @JsonbProperty("species_id")
    @Override
    public void setSpeciesId(CURIEBean species_id) {
        this.species_id = species_id;
    }
    
    @JsonbProperty("chr")
    @Override
    public String getChromosome() {
        return chromosome;
    }
    
    @JsonbProperty("chr")
    @Override
    public void setChromosome(String chromosome) {
        this.chromosome = chromosome;
    }
    
    @Override
    public CytobandIntervalBean getInterval() {
        return interval;
    }
    
    @Override
    public void setInterval(CytobandIntervalBean interval) {
        this.interval = interval;
    }
}

/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.jsonb;

import es.bsc.inb.ga4gh.vrs.model.AbsoluteCopyNumber;
import jakarta.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class AbsoluteCopyNumberBean 
        implements AbsoluteCopyNumber<LocationInterface>, VariationInterface {
        
    private String id;
    private LocationInterface subject;
        
    @Override
    @JsonbProperty("_id")
    public String getId() {
        return id;
    }
    
    @Override
    @JsonbProperty("_id")
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public LocationInterface getSubject() {
        return subject;
    }
    
    @Override
    public void setSubject(LocationInterface subject) {
        this.subject = subject;
    }
}

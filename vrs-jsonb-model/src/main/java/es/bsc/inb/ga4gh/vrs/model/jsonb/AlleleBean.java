/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.jsonb;

import es.bsc.inb.ga4gh.vrs.model.Allele;
import es.bsc.inb.ga4gh.vrs.model.jsonb.adapter.AlleleLocationDeserializer;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbTypeDeserializer;

/**
 * The state of a molecule at a Location.
 * 
 * @author Dmitry Repchevsky
 */

public class AlleleBean 
        implements Allele<CURIEBean, AlleleLocationInterface, SequenceExpressionInterface>,
        MolecularVariationInterface, HaplotypeMemberInterface {
    
    private CURIEBean id;
    
    private AlleleLocationInterface location;
    private SequenceExpressionInterface state;
    
    @JsonbProperty("_id")
    @Override
    public CURIEBean getId() {
        return id;
    }
    
    @JsonbProperty("_id")
    @Override
    public void setId(CURIEBean id) {
        this.id = id;
    }
    
    @JsonbTypeDeserializer(AlleleLocationDeserializer.class)
    @Override
    public AlleleLocationInterface getLocation() {
        return location;
    }
    
    @Override
    public void setLocation(AlleleLocationInterface location) {
        this.location = location;
    }
    
    @Override
    public SequenceExpressionInterface getState() {
        return state;
    }
    
    @Override
    public void setState(SequenceExpressionInterface state) {
        this.state = state;
    }
}

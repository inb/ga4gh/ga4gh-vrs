/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.jsonb.adapter;

import es.bsc.inb.ga4gh.vrs.model.jsonb.CURIEBean;
import jakarta.json.Json;
import jakarta.json.JsonValue;
import jakarta.json.bind.serializer.DeserializationContext;
import jakarta.json.bind.serializer.JsonbDeserializer;
import jakarta.json.stream.JsonParser;
import java.lang.reflect.Type;
import java.util.Collections;
import es.bsc.inb.ga4gh.vrs.model.jsonb.AlleleLocationInterface;
import es.bsc.inb.ga4gh.vrs.model.jsonb.LocationInterface;

/**
 * Allele.location property deserializer. The deserializer has to choose between
 * the 'CURIE' (which is actually a string) and the 'Location' which is an object.
 * 
 * "Allele": {
 *   
 * }
 * 
 * @author Dmitry Repchevsky
 */

public class AlleleLocationDeserializer implements JsonbDeserializer<AlleleLocationInterface> {

    @Override
    public AlleleLocationInterface deserialize(JsonParser jp, DeserializationContext dc, Type type) {
        final JsonValue v = jp.getValue();
        if (v != null) {
            switch(v.getValueType()) {
                case STRING: return new CURIEBean(jp.getString());
                case OBJECT: return dc.deserialize(LocationInterface.class, 
                        Json.createParserFactory(Collections.EMPTY_MAP)
                                .createParser(v.asJsonObject()));
            }
        }
        return null;
    }
}

/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.test;

import es.bsc.inb.ga4gh.vrs.model.ChromosomeLocation;
import es.bsc.inb.ga4gh.vrs.model.CytobandInterval;
import es.bsc.inb.ga4gh.vrs.model.jsonb.CURIEBean;
import es.bsc.inb.ga4gh.vrs.model.jsonb.ChromosomeLocationBean;
import es.bsc.inb.ga4gh.vrs.model.jsonb.CytobandIntervalBean;
import jakarta.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class ChromosomeLocationTest {

    public final static String CHROMOSOME_LOCATION_FILE = "ChromosomeLocation.json";
    
    @Test
    public void deserialization_test() {
        try (InputStream in = ChromosomeLocationTest.class.getClassLoader().getResourceAsStream(CHROMOSOME_LOCATION_FILE)) {
            final ChromosomeLocationBean location = JsonbBuilder.create().fromJson(in, ChromosomeLocationBean.class);
            if (location == null) {
                Assert.fail("ChromosomeLocation object is 'null'");
            }
            Assert.assertTrue("invalid location type", location instanceof ChromosomeLocation);
            //Assert.assertEquals("invalid 'type' value", "ChromosomeLocation", location.getType());
            
            final CURIEBean _id = location.getId();
            if (_id == null) {
                Assert.fail("'_id' value is 'null'");
            }
            Assert.assertEquals("invalid '_id' value", "loc:xyz", _id.getValue());
            final CURIEBean species_id = location.getSpeciesId();
            if (species_id == null) {
                Assert.fail("'species_id' value is 'null'");
            }
            Assert.assertEquals("invalid 'species_id' value", "taxonomy:9606", species_id.getValue());
            Assert.assertEquals("invalid 'chr' value", "1", location.getChromosome());
            
            final CytobandIntervalBean interval = location.getInterval();
            if (interval == null) {
                Assert.fail("'interval' value is 'null'");
            }
            Assert.assertTrue("invalid interval.type", interval instanceof CytobandInterval);
            //Assert.assertEquals("invalid 'interval.type' value", "CytobandInterval", interval.getType());
            Assert.assertEquals("invalid 'interval.start' value", "q22.2", interval.getStart());
            Assert.assertEquals("invalid 'interval.end' value", "q22.3", interval.getEnd());

        } catch(IOException ex) {
            Assert.fail(ex.getMessage());
        }
    }
}

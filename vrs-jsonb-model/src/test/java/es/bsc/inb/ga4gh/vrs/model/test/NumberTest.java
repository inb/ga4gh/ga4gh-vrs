/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.test;

import es.bsc.inb.ga4gh.vrs.model.jsonb.NumberBean;
import es.bsc.inb.ga4gh.vrs.model.jsonb.RangeNumberInterface;
import jakarta.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class NumberTest {
    public final static String NUMBER_FILE = "Number.json";
    
    @Test
    public void deserialization_test() {

        try (InputStream in = NumberTest.class.getClassLoader().getResourceAsStream(NUMBER_FILE)) {
            final RangeNumberInterface range_number = JsonbBuilder.create().fromJson(in, RangeNumberInterface.class);
            if (range_number == null) {
                Assert.fail("Number object is 'null'");
            }
            Assert.assertTrue("invalid number type", range_number instanceof NumberBean);

            final NumberBean number = (NumberBean)range_number;

        } catch(IOException ex) {
            Assert.fail(ex.getMessage());
        }
    }
}

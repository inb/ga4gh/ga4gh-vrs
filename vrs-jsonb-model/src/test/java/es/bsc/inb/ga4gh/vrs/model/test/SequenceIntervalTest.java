/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.test;

import es.bsc.inb.ga4gh.vrs.model.SequenceInterval;
import es.bsc.inb.ga4gh.vrs.model.jsonb.SequenceLocationIntervalInterface;
import jakarta.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class SequenceIntervalTest {
    public final static String SEQUENCE_INTERVAL_FILE = "SequenceInterval.json";
    
    @Test
    public void deserialization_test() {

        try (InputStream in = SimpleIntervalTest.class.getClassLoader().getResourceAsStream(SEQUENCE_INTERVAL_FILE)) {
            final SequenceLocationIntervalInterface interval = JsonbBuilder.create().fromJson(in, SequenceLocationIntervalInterface.class);
            if (interval == null) {
                Assert.fail("SequenceInterval object is 'null'");
            }
            Assert.assertTrue("invalid location type", interval instanceof SequenceInterval);

            final SequenceInterval sequence_interval = (SequenceInterval)interval;

        } catch(IOException ex) {
            Assert.fail(ex.getMessage());
        }
    }
}

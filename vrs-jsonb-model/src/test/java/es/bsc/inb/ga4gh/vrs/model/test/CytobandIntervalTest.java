package es.bsc.inb.ga4gh.vrs.model.test;

import es.bsc.inb.ga4gh.vrs.model.CytobandInterval;
import es.bsc.inb.ga4gh.vrs.model.jsonb.CytobandIntervalBean;
import jakarta.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class CytobandIntervalTest {

    public final static String CYTOBAND_INTERVAL_FILE = "CytobandInterval.json";
    
    @Test
    public void deserialization_test() {
        try (InputStream in = CytobandIntervalTest.class.getClassLoader().getResourceAsStream(CYTOBAND_INTERVAL_FILE)) {
            final CytobandIntervalBean interval = JsonbBuilder.create().fromJson(in, CytobandIntervalBean.class);
            if (interval == null) {
                Assert.fail("CytobandInterval object is 'null'");
            }
            //Assert.assertEquals("invalid 'type' value", "CytobandInterval", interval.getType());
            Assert.assertTrue("invalid type", interval instanceof CytobandInterval);
            Assert.assertEquals("invalid 'start' value", "q22.2", interval.getStart());
            Assert.assertEquals("invalid 'end' value", "q22.3", interval.getEnd());

        } catch(IOException ex) {
            Assert.fail(ex.getMessage());
        }
    }
}

/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.test;

import es.bsc.inb.ga4gh.vrs.model.SimpleInterval;
import es.bsc.inb.ga4gh.vrs.model.jsonb.SimpleIntervalBean;
import jakarta.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class SimpleIntervalTest {
    public final static String SIMPLE_INTERVAL_FILE = "SimpleInterval.json";
    
    @Test
    public void deserialization_test() {

        try (InputStream in = SimpleIntervalTest.class.getClassLoader().getResourceAsStream(SIMPLE_INTERVAL_FILE)) {
            final SimpleIntervalBean interval = JsonbBuilder.create().fromJson(in, SimpleIntervalBean.class);
            if (interval == null) {
                Assert.fail("SimpleInterval object is 'null'");
            }
            Assert.assertTrue("invalid interval type", interval instanceof SimpleInterval);
            //Assert.assertEquals("invalid 'type' value", "SimpleInterval", interval.getType());
            Assert.assertEquals("invalid 'start' value", Integer.valueOf(11), interval.getStart());
            Assert.assertEquals("invalid 'end' value", Integer.valueOf(22), interval.getEnd());

        } catch(IOException ex) {
            Assert.fail(ex.getMessage());
        }
    }
}

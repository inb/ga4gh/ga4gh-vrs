/**
 * *****************************************************************************
 * Copyright (C) 2023 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.test;

import es.bsc.inb.ga4gh.vrs.model.jsonb.CURIEBean;
import es.bsc.inb.ga4gh.vrs.model.jsonb.GeneBean;
import jakarta.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class CURIETest {
    
    public final static String GENE_FILE = "Gene.json";
    
    @Test
    public void deserialization_test() {
        try (InputStream in = CURIETest.class.getClassLoader().getResourceAsStream(GENE_FILE)) {
            final GeneBean gene = JsonbBuilder.create().fromJson(in, GeneBean.class);
            if (gene == null) {
                Assert.fail("gene object is 'null'");
            }
            final CURIEBean gene_id = gene.getGeneId();
            if (gene_id == null) {
                Assert.fail("'gene_id' value is 'null'");
            }
            Assert.assertEquals("invalid 'gene_id' value", "ncbigene:100010", gene_id.getValue());

        } catch(IOException ex) {
            Assert.fail(ex.getMessage());
        }
    }
}

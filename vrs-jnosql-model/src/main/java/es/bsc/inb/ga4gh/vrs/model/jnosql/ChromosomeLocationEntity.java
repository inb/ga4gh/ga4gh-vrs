/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.jnosql;

import es.bsc.inb.ga4gh.vrs.model.ChromosomeLocation;
import es.bsc.inb.ga4gh.vrs.model.jnosql.converter.CURIEValueConverter;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.nosql.Column;
import jakarta.nosql.Convert;
import jakarta.nosql.DiscriminatorValue;
import jakarta.nosql.Entity;

/**
 * @author Dmitry Repchevsky
 */

@Entity(ChromosomeLocation.TYPE)
@DiscriminatorValue(ChromosomeLocation.TYPE)
public class ChromosomeLocationEntity 
        implements ChromosomeLocation<CURIEEntity, CytobandIntervalEntity>,
        LocationInterface {
    
    @Column("_id")
    @Convert(CURIEValueConverter.class)
    private CURIEEntity id;
    
    @Column("species_id")
    private CURIEEntity species_id;
    
    @Column("chromosome")
    private String chromosome;
    
    @Column("interval")
    private CytobandIntervalEntity interval;
    
    @Override
    @JsonbProperty("_id")
    public CURIEEntity getId() {
        return id;
    }
    
    @Override
    @JsonbProperty("_id")
    public void setId(CURIEEntity id) {
        this.id = id;
    }
    
    @Override
    @JsonbProperty("species_id")
    public CURIEEntity getSpeciesId() {
        return species_id;
    }
    
    @Override
    @JsonbProperty("species_id")
    public void setSpeciesId(CURIEEntity species_id) {
        this.species_id = species_id;
    }
    
    @Override
    @JsonbProperty("chr")
    public String getChromosome() {
        return chromosome;
    }
    
    @Override
    @JsonbProperty("chr")
    public void setChromosome(String chromosome) {
        this.chromosome = chromosome;
    }
    
    @Override
    public CytobandIntervalEntity getInterval() {
        return interval;
    }
    
    @Override
    public void setInterval(CytobandIntervalEntity interval) {
        this.interval = interval;
    }
}

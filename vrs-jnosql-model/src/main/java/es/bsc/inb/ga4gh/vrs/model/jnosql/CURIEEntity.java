/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.jnosql;

import es.bsc.inb.ga4gh.vrs.model.CURIE;
import es.bsc.inb.ga4gh.vrs.model.Countable;
import es.bsc.inb.ga4gh.vrs.model.jnosql.adapter.CURIEDeserializer;
import es.bsc.inb.ga4gh.vrs.model.jnosql.adapter.CURIESerializer;
import jakarta.json.bind.annotation.JsonbTypeDeserializer;
import jakarta.json.bind.annotation.JsonbTypeSerializer;

/**
 * @author Dmitry Repchevsky
 */

@JsonbTypeSerializer(CURIESerializer.class)
@JsonbTypeDeserializer(CURIEDeserializer.class)
public class CURIEEntity implements CURIE, AlleleLocationInterface, 
        AbstractVariationInterface, Countable {

    private String value;
    
    public CURIEEntity() {
        value = "";
    }
    
    public CURIEEntity(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}

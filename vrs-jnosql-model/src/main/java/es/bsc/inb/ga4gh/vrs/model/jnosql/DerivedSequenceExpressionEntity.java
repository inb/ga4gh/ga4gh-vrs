/**
 * *****************************************************************************
 * Copyright (C) 2024 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.vrs.model.jnosql;

import es.bsc.inb.ga4gh.vrs.model.DerivedSequenceExpression;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.nosql.Column;
import jakarta.nosql.DiscriminatorValue;
import jakarta.nosql.Entity;

/**
 * @author Dmitry Repchevsky
 */

@Entity(DerivedSequenceExpression.TYPE)
@DiscriminatorValue(DerivedSequenceExpression.TYPE)
public class DerivedSequenceExpressionEntity 
        implements DerivedSequenceExpression<SequenceLocationEntity>, 
        SingleSequenceExpressionInterface {
    
    @Column("location")
    private SequenceLocationEntity location;
    @Column("reverse_complement")
    private Boolean reverse_complement;
    
    @Override
    public SequenceLocationEntity getLocation() {
        return location;
    }
    
    @Override
    public void setLocation(SequenceLocationEntity location) {
        this.location = location;
    }
    
    @JsonbProperty("reverse_complement")
    @Override
    public Boolean getReverseComplement() {
        return reverse_complement;
    }
    
    @JsonbProperty("reverse_complement")
    @Override
    public void setReverseComplement(Boolean reverse_complement) {
        this.reverse_complement = reverse_complement;
    }
}
